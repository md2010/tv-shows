﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ6
{
    class EmptyInputException : Exception
    {
        
        public EmptyInputException() : base("Search box is empty!") { }
        public EmptyInputException(String message) : base(message) { }
        public EmptyInputException(String message, Exception inner) : base(message, inner) { }
        
    }

    class NotChosenException : Exception
    {
        public NotChosenException() : base("Click on the show to see it's informations!") { }
        public NotChosenException(String message) : base(message) { }
        public NotChosenException(String message, Exception inner) : base(message, inner) { }
    }
   
}

