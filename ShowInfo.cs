﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ6
{
    class ShowInfo
    {
        public string id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string language { get; set; }
        public List<string> genres { get; set; }
        public string status { get; set; }
        public string runtime { get; set; }
        public string premiered { get; set; }
        public string officialSite { get; set; }
        public scheduleType schedule { get; set; }
        public ratingType rating { get; set; }

        public string weight { get; set; }
        public networkType network { get; set; }
        public string webChannel { get; set; }
        public externalsType externals { get; set; }
        public imageType image { get; set; }
        public string summary { get; set; }
        public string updated { get; set; }
        public linkType _links { get; set; }

    }

    class scheduleType
    {
        public string time { get; set; }
        public List<string> days { get; set; }
    }

    class ratingType
    {
        public string average { get; set; }
    }

    class networkType
    {
        public string id { get; set; }
        public string name { get; set; }
        public countryType country { get; set; }
    }

    class countryType
    {
        public string name { get; set; }
        public string code { get; set; }
        public string timezone { get; set; }
    }

    class externalsType
    {
        public string tvrage { get; set; }
        public string thetvdb { get; set; }
        public string imdb { get; set; }
    }

    class imageType
    {
        public string medium { get; set; }
        public string original { get; set; }
    }

    class linkType
    {
        public selfType self { get; set; }
        public prevEpType previousepisode { get; set; }
    }

    class selfType
    {
    public string href { get; set; }
    }

    class prevEpType
    {
        public string href { get; set; }
    }
}
