﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DZ6
{
    public partial class Form1 : Form
    {
        List<int> IDs = new List<int>();
        List<string> names = new List<string>();
        bool search_clicked = false;
        int selected_showID;
        dynamic dynamicObject;
        dynamic SeasonObj;
        dynamic EpisodeObj;
        dynamic CastObj;
        dynamic CrewObj;
        public Form1()
        {
            InitializeComponent();          
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button_search_Click(object sender, EventArgs e)
        {
            search_clicked = true;
            this.Shows.Items.Clear();
            string first_part_url = "http://api.tvmaze.com/search/shows?q=";
            string second_part_url = textBox_search.Text.ToString();
            try
            {
                Check(second_part_url);
            }
            catch (EmptyInputException ex)
            {
                MessageBox.Show(ex.Message);
            }
            string url = first_part_url + second_part_url;
            ConvertShows_json(url);
        }

        public void Check(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                throw new EmptyInputException();
            }
        }

        public void ConvertShows_json(string url)
        {
            //loading json
            string json;
            using (WebClient client = new WebClient())
            {
                json = (client.DownloadString(url));
            }
            dynamic items = JsonConvert.DeserializeObject(json);
            foreach (dynamic item in items)
            {
                JObject jsonObject = item.show;
                string show_name = (string)jsonObject.SelectToken("name");
                names.Add(show_name);
                Shows.Items.Add(show_name);
                IDs.Add((int)jsonObject.SelectToken("id"));
            }
        }

        public void findID()
        {
            string selected_show;
            try
            {
                IsChosen();
            }
            catch (NotChosenException ex)
            {
                MessageBox.Show(ex.Message);
            }
            selected_show = Shows.SelectedItem.ToString();
            for (int i = 0; i < names.Count; i++)
            {
                if (names[i] == selected_show)
                {
                    selected_showID = IDs[i];
                    break;
                }
            } 
        }

        public void IsChosen()
        {
            if (Shows.SelectedIndex == -1)
            {
                throw new NotChosenException();
            }
        }

        private void ShowInfoButton_Click(object sender, EventArgs e)
        {
            if (search_clicked == true)
                findID();
            string url = "http://api.tvmaze.com/shows/" + selected_showID.ToString();
            ConvertInfo(url);
            display_info();
            display_ShowImage();
            search_clicked = false;
        }

        public void ConvertInfo(string url)
        {
            string json;
            using (WebClient client = new WebClient())
            {
                json = (client.DownloadString(url));
            }
            dynamicObject = JsonConvert.DeserializeObject<ShowInfo>(json);
        }

        public void display_info()
        {
            Shows.Items.Clear();
            string display_name = "NAME: " + dynamicObject.name;
            Shows.Items.Add(display_name);

            string display_id = "SHOW ID: " + dynamicObject.id;
            Shows.Items.Add(display_id);

            string display_type = "TYPE: " + dynamicObject.type;
            Shows.Items.Add(display_type);

            string display_language = "LANGUAGE: " + dynamicObject.language;
            Shows.Items.Add(display_language);

            string display_genres = "GENRES: ";
            foreach (string s in dynamicObject.genres)
            {
                display_genres += s;
                display_genres += " ";
            }
            Shows.Items.Add(display_genres);

            string display_status = "STATUS: " + dynamicObject.status;
            Shows.Items.Add(display_status);

            string display_runtime = "RUNTIME: " + dynamicObject.runtime;
            Shows.Items.Add(display_runtime);

            string display_premiered = "PREMIERED: " + dynamicObject.premiered;
            Shows.Items.Add(display_premiered);

            string display_officialSite = "OFFICIAL SITE: " + dynamicObject.officialSite;
            Shows.Items.Add(display_officialSite);

            string display_schedule = "SCHEDULE:  " + dynamicObject.schedule.time + ", ";
            foreach (string s in dynamicObject.schedule.days)
            {
                display_schedule += s;
                display_schedule += " ";
            }
            Shows.Items.Add(display_schedule);

            string display_rating = "AVERAGE RATING: " + dynamicObject.rating.average;
            Shows.Items.Add(display_rating);

            string display_weight = "WEIGHT: " + dynamicObject.weight;
            Shows.Items.Add(display_weight);

            string display_network = "NETWORK:";
            Shows.Items.Add(display_network);
            string display_networkInfo = "    ID: " + dynamicObject.network.id + "  Name: " + dynamicObject.network.name;
            string display_countryInfo = "    Country: " + dynamicObject.network.country.name + " - code: " + dynamicObject.network.country.code + ", timezone: " + dynamicObject.network.country.timezone;
            Shows.Items.Add(display_networkInfo);
            Shows.Items.Add(display_countryInfo);

            string display_webChannel = "WEB CHANNEL: " + dynamicObject.webChannel;
            Shows.Items.Add(display_webChannel);

            string display_externals = "EXTERNALS:  tvrage: " + dynamicObject.externals.tvrage + "  thetvdb: " + dynamicObject.externals.thetvdb + "  imdb: " + dynamicObject.externals.imdb;
            Shows.Items.Add(display_externals);

            string display_image = "URL to original image: " + dynamicObject.image.original;
            Shows.Items.Add(display_image);
            this.pictureBox2.Load(dynamicObject.image.medium);

            string display_summary = "SUMMARY: Double click to see the summary.";
            Shows.Items.Add(display_summary);

            string display_updated = "UPDATED: " + dynamicObject.updated;
            Shows.Items.Add(display_updated);

            string display_links = "LINKS:  Self: " + dynamicObject._links.self.href;
            Shows.Items.Add(display_links);
            string display_links1 = "           Previous episode: " + dynamicObject._links.previousepisode.href;
            Shows.Items.Add(display_links1);
        }

        private void SeasonsButton_Click(object sender, EventArgs e)
        {
            if (search_clicked == true)
                findID();
            search_clicked = false;
            string url = "http://api.tvmaze.com/shows/" + selected_showID + "/seasons";
            convertSeasons(url);
            display_ShowImage();
        }

        public void convertSeasons(string url)
        {
            Shows.Items.Clear();

            string json;
            using (WebClient client = new WebClient())
            {
                json = (client.DownloadString(url));
            }
            SeasonObj = JsonConvert.DeserializeObject(json);
            foreach (dynamic item in SeasonObj)
            {
                string S_num = "SEASON " + item.number;
                Shows.Items.Add(S_num);

                string ep_order = "    Episode order: " + item.episodeOrder;
                Shows.Items.Add(ep_order);

                string premiere = "    Premiere date: " + item.premiereDate;
                Shows.Items.Add(premiere);

                string end = "    End date: " + item.endDate;
                Shows.Items.Add(end);

                JObject jsonObject = item.network;
                string net_name = (string)jsonObject.SelectToken("name");
                string network = "    Network: " + net_name;
                Shows.Items.Add(network);

                string web_url = "    URL: " + item.url;
                Shows.Items.Add(web_url);
            }

        }

        private void EpisodesButton_Click(object sender, EventArgs e)
        {
            if (search_clicked == true)
                findID();
            search_clicked = false;
            string url = "http://api.tvmaze.com/seasons/" + selected_showID + "/episodes";
            convertEpisodes(url);
            display_ShowImage();
        }

        public void convertEpisodes(string url)
        {
            Shows.Items.Clear();

            string json;
            using (WebClient client = new WebClient())
            {
                json = (client.DownloadString(url));
            }
            EpisodeObj = JsonConvert.DeserializeObject(json);
            foreach (dynamic item in EpisodeObj)
            {
                string e_name = "EPISODE: " + item.name;
                Shows.Items.Add(e_name);

                string s_num = "    Season: " + item.season;
                Shows.Items.Add(s_num);

                if (item.number == null)
                    item.number = "Special";
                string e_num = "    Episode " + item.number;
                Shows.Items.Add(e_num);

                string date = "    Air date: " + item.airdate;
                Shows.Items.Add(date);

                string time = "    Air time: " + item.airtime;
                Shows.Items.Add(time);

                string runTime = "    Run Time: " + item.runtime;
                Shows.Items.Add(runTime);

                string web_url = "    URL: " + item.url;
                Shows.Items.Add(web_url);
            }
        }

        private void CastButton_Click(object sender, EventArgs e)
        {
            if (search_clicked == true)
                findID();
            search_clicked = false;
            string url = "http://api.tvmaze.com/shows/" + selected_showID + "/cast";
            search_clicked = false;
            convertCast(url);
            display_ShowImage();
        }

        public void convertCast(string url)
        {
            Shows.Items.Clear();
            string json;
            using (WebClient client = new WebClient())
            {
                json = (client.DownloadString(url));
            }
            CastObj = JsonConvert.DeserializeObject(json);
            foreach (dynamic item in CastObj)
            {
                JObject jsonObject = item.person;
                string name = (string)jsonObject.SelectToken("name");

                JObject jsonObject1 = item.character;
                string name1 = (string)jsonObject1.SelectToken("name");

                string displayWhoIsWho = name1 + "\t\t\t" + name;
                Shows.Items.Add(displayWhoIsWho);
            }

        }
        public void display_ShowImage()
        {
            this.pictureBox2.Visible = true;
            string url = "http://api.tvmaze.com/shows/" + selected_showID.ToString();
            ConvertInfo(url);           
            this.pictureBox2.Load(dynamicObject.image.medium);
        }


        private void CrewButton__Click(object sender, EventArgs e)
        {
            if (search_clicked == true)
                findID();
            search_clicked = false;
            string url = "http://api.tvmaze.com/shows/" + selected_showID + "/crew";
            convertCrew(url);
            display_ShowImage();
        }

        public void convertCrew(string url)
        {
            Shows.Items.Clear();
            string json;
            using (WebClient client = new WebClient())
            {
                json = (client.DownloadString(url));
            }
            CrewObj = JsonConvert.DeserializeObject(json);
            foreach (dynamic item in CrewObj)
            {
                string display_type = "    " + item.type;

                JObject jsonObject = item.person;
                string display_name = (string)jsonObject.SelectToken("name");
                string display_bday = "    " + (string)jsonObject.SelectToken("birthday");               

                Shows.Items.Add(display_name);
                Shows.Items.Add(display_type);
                Shows.Items.Add(display_bday);
                Shows.Items.Add(" ");
            }
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Shows_SelectedIndexChanged(object sender, EventArgs e)
        {
            string s = Shows.SelectedItem.ToString();
            if (s.Contains("SUMMARY"))
            {
                string display_summary = "SUMMARY: " + dynamicObject.summary;
                MessageBox.Show(display_summary);               
            }
        }
       
    }
}
