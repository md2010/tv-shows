﻿namespace DZ6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textBox_search = new System.Windows.Forms.TextBox();
            this.button_search = new System.Windows.Forms.Button();
            this.Shows = new System.Windows.Forms.ListBox();
            this.ShowInfoButton = new System.Windows.Forms.Button();
            this.SeasonsButton = new System.Windows.Forms.Button();
            this.EpisodesButton = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.CastButton = new System.Windows.Forms.Button();
            this.CrewButton_ = new System.Windows.Forms.Button();
            this.QuitButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_search
            // 
            this.textBox_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_search.ForeColor = System.Drawing.Color.Silver;
            this.textBox_search.Location = new System.Drawing.Point(21, 24);
            this.textBox_search.Name = "textBox_search";
            this.textBox_search.Size = new System.Drawing.Size(342, 30);
            this.textBox_search.TabIndex = 0;
            this.textBox_search.Text = "Search...";
            // 
            // button_search
            // 
            this.button_search.Image = ((System.Drawing.Image)(resources.GetObject("button_search.Image")));
            this.button_search.Location = new System.Drawing.Point(380, 18);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(59, 48);
            this.button_search.TabIndex = 1;
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.button_search_Click);
            // 
            // Shows
            // 
            this.Shows.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Shows.FormattingEnabled = true;
            this.Shows.ItemHeight = 20;
            this.Shows.Location = new System.Drawing.Point(12, 97);
            this.Shows.Name = "Shows";
            this.Shows.Size = new System.Drawing.Size(1030, 304);
            this.Shows.TabIndex = 2;
            this.Shows.SelectedIndexChanged += new System.EventHandler(this.Shows_SelectedIndexChanged);
            // 
            // ShowInfoButton
            // 
            this.ShowInfoButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ShowInfoButton.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ShowInfoButton.Location = new System.Drawing.Point(12, 447);
            this.ShowInfoButton.Name = "ShowInfoButton";
            this.ShowInfoButton.Size = new System.Drawing.Size(139, 67);
            this.ShowInfoButton.TabIndex = 3;
            this.ShowInfoButton.Text = "Info";
            this.ShowInfoButton.UseVisualStyleBackColor = false;
            this.ShowInfoButton.Click += new System.EventHandler(this.ShowInfoButton_Click);
            // 
            // SeasonsButton
            // 
            this.SeasonsButton.BackColor = System.Drawing.Color.MistyRose;
            this.SeasonsButton.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SeasonsButton.Location = new System.Drawing.Point(211, 447);
            this.SeasonsButton.Name = "SeasonsButton";
            this.SeasonsButton.Size = new System.Drawing.Size(142, 67);
            this.SeasonsButton.TabIndex = 4;
            this.SeasonsButton.Text = "Seasons";
            this.SeasonsButton.UseVisualStyleBackColor = false;
            this.SeasonsButton.Click += new System.EventHandler(this.SeasonsButton_Click);
            // 
            // EpisodesButton
            // 
            this.EpisodesButton.BackColor = System.Drawing.Color.Tan;
            this.EpisodesButton.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EpisodesButton.Location = new System.Drawing.Point(425, 447);
            this.EpisodesButton.Name = "EpisodesButton";
            this.EpisodesButton.Size = new System.Drawing.Size(134, 67);
            this.EpisodesButton.TabIndex = 5;
            this.EpisodesButton.Text = "Episodes";
            this.EpisodesButton.UseVisualStyleBackColor = false;
            this.EpisodesButton.Click += new System.EventHandler(this.EpisodesButton_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(1081, 73);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(287, 357);
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // CastButton
            // 
            this.CastButton.BackColor = System.Drawing.Color.LightGreen;
            this.CastButton.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CastButton.Location = new System.Drawing.Point(633, 447);
            this.CastButton.Name = "CastButton";
            this.CastButton.Size = new System.Drawing.Size(134, 67);
            this.CastButton.TabIndex = 8;
            this.CastButton.Text = "Cast";
            this.CastButton.UseVisualStyleBackColor = false;
            this.CastButton.Click += new System.EventHandler(this.CastButton_Click);
            // 
            // CrewButton_
            // 
            this.CrewButton_.BackColor = System.Drawing.Color.Khaki;
            this.CrewButton_.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CrewButton_.Location = new System.Drawing.Point(857, 447);
            this.CrewButton_.Name = "CrewButton_";
            this.CrewButton_.Size = new System.Drawing.Size(134, 68);
            this.CrewButton_.TabIndex = 9;
            this.CrewButton_.Text = "Crew";
            this.CrewButton_.UseVisualStyleBackColor = false;
            this.CrewButton_.Click += new System.EventHandler(this.CrewButton__Click);
            // 
            // QuitButton
            // 
            this.QuitButton.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.QuitButton.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.QuitButton.Location = new System.Drawing.Point(1271, 493);
            this.QuitButton.Name = "QuitButton";
            this.QuitButton.Size = new System.Drawing.Size(118, 45);
            this.QuitButton.TabIndex = 10;
            this.QuitButton.Text = "Quit";
            this.QuitButton.UseVisualStyleBackColor = false;
            this.QuitButton.Click += new System.EventHandler(this.QuitButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1401, 550);
            this.Controls.Add(this.QuitButton);
            this.Controls.Add(this.CrewButton_);
            this.Controls.Add(this.CastButton);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.EpisodesButton);
            this.Controls.Add(this.SeasonsButton);
            this.Controls.Add(this.ShowInfoButton);
            this.Controls.Add(this.Shows);
            this.Controls.Add(this.button_search);
            this.Controls.Add(this.textBox_search);
            this.Name = "Form1";
            this.Text = "TV Shows ";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_search;
        private System.Windows.Forms.Button button_search;
        private System.Windows.Forms.ListBox Shows;
        private System.Windows.Forms.Button ShowInfoButton;
        private System.Windows.Forms.Button SeasonsButton;
        private System.Windows.Forms.Button EpisodesButton;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button CastButton;
        private System.Windows.Forms.Button CrewButton_;
        private System.Windows.Forms.Button QuitButton;
    }
}

